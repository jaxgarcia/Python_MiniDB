user = {}
userTxt = open("Profiles", "a+")

def initializeDict():
    with open("Profiles", "r") as f:
        for line in f:
            user.update(eval(line))
    return user

def viewProfile(un):
    print("====================================")
    print("Your Profile:")
    for key, value in user.items():
        if(un == key):
            print("Name: %s %s" % (value[1], value[2]))
            print("Birthday: %s" % value[3])
            print("Hobby: %s" % value[4])

    signOut = input("\nPress 1 to sign-out: ")

    if(signOut == str(1)):
        print("====================================")
        mainMenu()
    else:
        print("Invalid input")
        viewProfile(un)

def signIn():
    user = initializeDict()
    print("SIGN IN")
    uName = input("Enter User Name: ")
    pWord = input("Enter Password: ")

    for key,value in user.items():
        if(uName == key) and (pWord == value[0]):
            viewProfile(uName)
            break
        elif(uName == key) or (pWord == value[0]):
            print("Username or Password is incorrect")
            print("====================================")
            signIn()
    else:
        print("\nUser does not exist")
        print("====================================")
        mainMenu()

def listUser():
    user = initializeDict()
    print("USER LIST")
    for key, value in user.items():
        print(key)

def addToDict(un,pw,fn,ln,bd,hb):
    newUser = {}
    userTxt = open("Profiles", "a+")
    newUser[un] = pw,fn,ln,bd,hb
    userTxt.write(str(newUser) + "\n")
    userTxt.close()

def signUp():
    try:
        user = initializeDict()
    except SyntaxError:
        pass

    print("CREATE USER")
    userName = input("Enter User Name: ")

    try:
        for key,values in user.items():
            if(userName == key):
                print("Username already exist. Choose another username")
                signUp()
    except UnboundLocalError:
        pass

    password = input("Enter Password: ")
    firstName = input("Enter First Name: ")
    lastName = input("Enter Last Name: ")
    bday = input("Enter birthday: ")
    hobby = input("Enter hobby: ")
    addToDict(userName,password,firstName,lastName,bday,hobby)
    print("\nUser, %s saved." % userName)

def mainMenu():
    print("Main Menu:\n1. Sign-in\n2. Sign-up\n3. View Users")
    choice = input("Enter choice: ")

    if(choice == str(1)):
        print("====================================")
        signIn()
    elif(choice == str(2)):
        print("====================================")
        signUp()
        print("====================================")
        mainMenu()
    elif(choice == str(3)):
        print("====================================")
        listUser()
        print("====================================")
        mainMenu()
    else:
        print("Invalid Input")
        print("====================================")
        mainMenu()

mainMenu()

